% 振り子parameter

%%
clear
clc
close all
%% parameter

gravity = -9.8; % 重力加速度[m/s2]
positionX = [-0.3 0.3]; % 振り子の位置[m]
length = [50 50 80]/100; % 振り子長さ[m]

%% display
disp('Parameter loaded')