# Lissajous Pendulum by Simscape Multibody

## Abstract

I would like to simulate simple physics experiments by Matlab.

This repository shows lissajous pendulum.

## Environment

- Matlab 2023b
- Simlink
- Simscape
- Simscape Multibody

## How to play

1. Git clone this reposity.
2. Open Matlab 2023b at this repository folder.
3. Open `lissajous_pendulum.slx` and play the simulation.

## Result

![Simulation movie](fig/lissajous_pendulum.gif)

![Simulation result](fig/lissajous_graph.png)

## Remarks

Overview of this model is blow.

![model](fig/overview.png)

It is important to set the small weight in the center(orange circle in the figure).
